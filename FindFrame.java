package student.view;

import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import student.dao.StuDao;
import student.model.Student;

/**
 * 这个类是处理查询操作的界面类
 */
public class FindFrame extends JFrame {// 继承父窗体类
	// 定义要用到学号 姓名 性别 出生日期 政治面貌 家庭住址 电话 宿舍号 八个标签
		JLabel jlnumber;
		JLabel jlname;
		JLabel jlsex;
		JLabel jlbirthday;
		JLabel jlpoliticalStatus;
		JLabel jlhomeAddress;
		JLabel jlphone;
		JLabel jldormitoryNum;
		// 定义要用到学号 姓名 性别 出生日期 政治面貌 家庭住址 电话 宿舍号 八个文本框
		JTextField jtnumber;
		JTextField jtname;
		JTextField jtsex;
		JTextField jtbirthday;
		JTextField jtpoliticalStatus;
		JTextField jthomeAddress;
		JTextField jtphone;
		JTextField jtdormitoryNum;
		// 两个按钮
		JButton buttonadd;
		JButton buttonreturn;
		// 定义要用到学号 姓名 性别 出生日期 政治面貌 家庭住址 电话 宿舍号 八个面板
		JPanel jpnumber;
		JPanel jpname;
		JPanel jpsex;
		JPanel jpbirthday;
		JPanel jppoliticalStatus;
		JPanel jphomeAddress;
		JPanel jpphone;
		JPanel jpdormitoryNum;
	// 用于放置按钮的面板
	JPanel jpforbutton;

	/*
	 * 定义初始化变量以及窗体
	 */
	public FindFrame() {
		// 初始化要用到学号 姓名 性别 出生日期 政治面貌 家庭住址 电话 宿舍号 八个标签
				jlnumber = new JLabel("学号：");
				jlname = new JLabel("姓名：");
				jlsex = new JLabel("性别：");
				jlbirthday = new JLabel("出生日期：");
				jlpoliticalStatus = new JLabel("政治面貌：");
				jlhomeAddress = new JLabel("家庭住址：");
				jlphone = new JLabel("电话：");
				jldormitoryNum = new JLabel("宿舍号：");
				// 初始化要用到学号 姓名 性别 出生日期 政治面貌 家庭住址 电话 宿舍号 八个文本框
				jtnumber = new JTextField("", 20);
				jtname = new JTextField("", 20);
				jtsex = new JTextField("", 20);
				jtbirthday = new JTextField("", 20);
				jtpoliticalStatus = new JTextField("", 20);
				jthomeAddress = new JTextField("", 20);
				jtphone = new JTextField("", 20);
				jtdormitoryNum = new JTextField("", 20);
				// 初始化两个按钮
				buttonadd = new JButton("添加");
				buttonreturn = new JButton("返回");
				// 初始化要用到学号 姓名 性别 出生日期  政治面貌 家庭住址 电话 宿舍号 八个面板
				jpnumber = new JPanel();
				jpname = new JPanel();
				jpsex = new JPanel();
				jpbirthday = new JPanel();
				jppoliticalStatus = new JPanel();
				jphomeAddress = new JPanel();
				jpphone = new JPanel();
				jpdormitoryNum = new JPanel();
				// 用于放置按钮的面板
				jpforbutton = new JPanel(new GridLayout(1, 1));
				// 学号面板添加学号标签和学号文本框
				jpnumber.add(jlnumber);
				jpnumber.add(jtnumber);
				// 姓名面板添加姓名标签和姓名文本框
				jpname.add(jlname);
				jpname.add(jtname);
				// 性别面板添加性别标签和性别文本框
				jpsex.add(jlsex);
				jpsex.add(jtsex);
				// 生日面板添加生日标签和生日文本框
				jpbirthday.add(jlbirthday);
				jpbirthday.add(jtbirthday);
				// 政治面貌面板添加政治面貌标签和政治面貌文本框
				jppoliticalStatus.add(jlpoliticalStatus);
				jppoliticalStatus.add(jtpoliticalStatus);
				// 家庭住址面板添加家庭住址标签和家庭住址文本框
				jphomeAddress.add(jlhomeAddress);
				jphomeAddress.add(jthomeAddress);
				// 电话面板添加电话标签和电话文本框
				jpphone.add(jlphone);
				jpphone.add(jtphone);
				// 宿舍号面板添加宿舍号标签和宿舍号文本框
				jpdormitoryNum .add(jldormitoryNum );
				jpdormitoryNum .add(jtdormitoryNum );
		// 按钮面板查询两个按钮
		jpforbutton.add(buttonadd);
		jpforbutton.add(buttonreturn);
		// 处理查询按钮点击事件
		// 处理返回按钮点击事件
		buttonadd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String number = jtnumber.getText().trim().toString();
				String name = jtname.getText().trim().toString();
				String sex = jtsex.getText().trim().toString();
				String birthday = jtbirthday.getText().trim().toString();
				String politicalStatus = jtpoliticalStatus.getText().trim().toString();
				String homeAddress = jthomeAddress.getText().trim().toString();
				String phone = jtphone.getText().trim().toString();
				String dormitoryNum = jtdormitoryNum.getText().trim().toString();
				if (number.equals("")) {
					JOptionPane.showMessageDialog(FindFrame.this, "学号不能为空");
				} else {
					// 调用操作数据库
					StuDao stuDao = new StuDao();
					if (stuDao.findStu(number) == null) {
						JOptionPane.showMessageDialog(FindFrame.this, "学号不存在");
					} else {
						Student student = stuDao.findStu(number);
						if ((student != null)) {
							jtname.setText(student.getName());
							jtsex.setText(student.getSex());
							jtbirthday.setText(student.getBirthday());
							jtpoliticalStatus.setText(student.getPoliticalStatus());
							jthomeAddress.setText(student.getHomeAddress());
							jtphone.setText(student.getPhone());
							jtdormitoryNum.setText(student.getDormitoryNum());
						} else {
							JOptionPane.showMessageDialog(FindFrame.this, "查询异常  请填写正确表单");
						}
					}

				}
			}
		});
		// 处理返回按钮点击事件
		buttonreturn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// 返回主窗体
				FindFrame.this.dispose();// 关闭当前窗口
				MainFrame window = new MainFrame();
			}
		});

		// 将五个面板使用网格布局方式查询到当前
		this.setLayout(new GridLayout(9, 1));
		this.add(jpnumber);
		this.add(jpname);
		this.add(jpsex);
		this.add(jpbirthday);
		this.add(jppoliticalStatus);
		this.add(jphomeAddress);
		this.add(jpphone);
		this.add(jpdormitoryNum);
		this.add(jpforbutton);
		// 窗口标题
		this.setTitle("查询學生信息");
		// 窗体大小
		this.setSize(500, 340);
		// 设置图标
		this.setIconImage((new ImageIcon("images/logo.jpg")).getImage());
		// 设置可关闭进程
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		// 获得屏幕宽度
		int width = Toolkit.getDefaultToolkit().getScreenSize().width;
		// 获得屏幕高度
		int height = Toolkit.getDefaultToolkit().getScreenSize().height;
		// 居中显示
		this.setLocation((width - 500) / 2, (height - 400) / 2);
		// 设置窗体可见
		this.setVisible(true);
		// 可改变窗体大小
		this.setResizable(false);
	}

}
